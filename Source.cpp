#define _CRT_SECURE_NO_WARNINGS

#include <iostream>

using namespace std;

int* countWordList(char* source, char** words, int length);
void displayCounter(int*, int length);
char** getWords(int);

int main()
{
	const int size = 256;
	char source[256] = "lorem ipsum, is simply"
		" printingipsum : % industry. lorem ipsum has been"
		" the, standard ipsum ofodod";

	int length = 3;
	char** words = getWords(length);

	int* counter = countWordList(source, words, length);
	displayCounter(counter, length);

	return 0;
}

char** getWords(int length)
{
	char** words = new char* [length];
	char temp[256] = "";

	for (int i = 0; i < length; i++)
	{
		cout << "Enter word: ";
		cin.getline(temp, 256, '\n');
		words[i] = new char[strlen(temp) + 1];

		strcpy(words[i], temp);
	}

	return words;
}

int* countWordList(char* source, char** words, int length)
{
	int* counter = new int [length];

	for (int i = 0; i < length; i++) {

		counter[i] = 0;

		char* pword = new char[strlen(words[i])];
		pword = strstr(source, words[i]);

		while (pword)
		{
			counter[i]++;

			pword = strstr(pword + length, words[i]);
		}

	}

	return counter;
}

void displayCounter(int* counter, int length)
{
	for (int i = 0; i < length; i++)
		std::cout << (i + 1) << ". " << counter[i] << std::endl;
}